<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Mail\AttendancePunchIn;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AttendanceController extends Controller
{
    public function list(Request $req){
        $date_in = $req->date_in; 
        $remarks = $req->remarks; 
        
        /*
        $attendances = Attendance::select('*');

        if (isset($req->date_in)){
            $attendances = $attendances->where(DB::raw("DATE_FORMAT(date_in,'%Y-%m-%d')"),$req->date_in);            
        }

        if (isset($req->remarks)){
            $attendances = $attendances->where('remarks','LIKE','%'.$req->remarks.'%');
        }

        $attendances = $attendances->orderBy('date_in')->paginate(2);
        */
        //--------------

        $attendances = Attendance::where(function($q) use ($remarks,$date_in){
            if (isset($date_in)){
                $q = $q->where(DB::raw("DATE_FORMAT(date_in,'%Y-%m-%d')"),$date_in);
            }
    
            if (isset($remarks)){
                $q = $q->where('remarks','LIKE','%'.$remarks.'%');
            }   
        })->orderBy('date_in')->paginate(20);

        return view('attendance.list',compact('attendances','date_in','remarks'));
    }

    public function capture($user=''){
        $attendance = new Attendance();
        $attendance->user_id = Auth::user()->id;
               
        // $mu = User::find(Auth::user()->id);
        // $data['first_name'] = $mu->name;
        $data['attendance'] = $attendance;
        return view('attendance.capture_attendance',$data);
    }

    public function save(Request $req){

        
        $validated = $req->validate([
            'remarks' => 'required'
        ],Attendance::myCustomMsg());

        $attachment = $req->attachment;
        $oriname = $attachment->getClientOriginalName();
        $ext = $attachment->getClientOriginalExtension();
        
        $newname = date('Y_m_y_h_i_s').".".$ext;
        
        $attendance = new Attendance();
        $attendance->remarks = $req->remarks;
        $attendance->date_in = date('Y-m-d H:i');//$req->date_in;
        $attendance->user_id = Auth::user()->id;
        if (!empty($attachment))
            $attendance->attachment = $newname;
        $attendance->save();

        if (!empty($attachment))
            $attachment->storeAs('public/attendance',$newname);
        
        Mail::to($req->user())->send(new AttendancePunchIn($attendance));

        return redirect('/attendance/list');
    }

    public function edit($id){
        $attendance = Attendance::find($id);
        // $mu = User::find(attendance->user_id);
        // $data['first_name'] = $mu->name;
        $data['attendance'] = $attendance;
        return view('attendance.capture_attendance',$data);
    }

    public function update($id, Request $req){

        $validated = $req->validate([
            'remarks' => 'required',
            'cat' => 'required',
        ],Attendance::myCustomMsg());

        $attachment = $req->attachment;
        $oriname = $attachment->getClientOriginalName();
        $ext = $attachment->getClientOriginalExtension();        
        $newname = date('Y_m_y_h_i_s').".".$ext;

        $attendance = Attendance::find($id);
        
        $oldattachment = $attendance->attachment;
        $oldremarks = $attendance->remarks;
        if (!empty($oldattachment)){
            if (Storage::disk('public/attendance/')->exists($oldattachment)) {
                Storage::disk('public/attendance/')->delete($oldattachment);
            }
        }

        $attendance->remarks = $req->remarks.'\n'.$oldremarks;
        // $attendance->date_in = date('Y-m-d H:i');//$req->date_in;
        // $attendance->user_id = 1;//$req->user_id;
        if (!empty($attachment))
            $attendance->attachment = $newname;

        $attendance->save();

        if (!empty($attachment))
            $attachment->storeAs('public/attendance',$newname);

        return redirect('/attendance/list');
    }
    
    public function delete($id){
        Attendance::find($id)->delete();
        return redirect('/attendance/list');
    }

    


    public function view(){
        
    }

    

    

    public function print(){
        
    }

    public function approve(){
        
    }
}
