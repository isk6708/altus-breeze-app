<?php

namespace App\Http\Controllers\api;

use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req){
        $date_in = $req->date_in; 
        $remarks = $req->remarks; 
        
        $attendances = Attendance::where(function($q) use ($remarks,$date_in){
            if (isset($date_in)){
                $q = $q->where(DB::raw("DATE_FORMAT(date_in,'%Y-%m-%d')"),$date_in);
            }
    
            if (isset($remarks)){
                $q = $q->where('remarks','LIKE','%'.$remarks.'%');
            }   
        })->orderBy('date_in')->get();

        return $attendances;
    }

    // public function store(Request $req)
    // {
    //     $validator = Validator::make($req->all(), [
    //         'remarks' => 'required'
    //     ],Attendance::myCustomMsg());

    //     if ($validator->fails()) {
    //         $status = (['status'=>'error']) ;
            
    //     }else{
    //         $attendance = new Attendance();
    //         $attendance->remarks = $req->remarks;
    //         $attendance->date_in = date('Y-m-d H:i');
    //         $attendance->user_id = 1;//Auth::user()->id;
    //         if ($attendance->save()){
    //             $status = (['status'=>'success']) ;
    //         }else{
    //             $status = (['status'=>'error']) ;
    //         }
    //     }
    //     return $status;
    // }


    /**
     * Update the specified resource in storage.
     */
    public function simpankemaskini(Request $req, string $id='')
    {
        if ($id == ''){
            $attendance = new Attendance();
        }else{
            $attendance = Attendance::find($id);
        }

        $validator = Validator::make($req->all(), [
            'remarks' => 'required'
        ],Attendance::myCustomMsg());


        if ($validator->fails()) {
            $status = (['status'=>'error','source'=>$validator]);
        }else{
            
            $attendance->remarks = $req->remarks;
            $attendance->date_in = date('Y-m-d H:i');//$req->date_in;
            $attendance->user_id = 1;//$req->user_id;
            $attendance->save();
            if ($attendance->save()){
                $status = (['status'=>'success']) ;
            }else{
                $status = (['status'=>'error']) ;
            }
        }
        return $status;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Attendance::find($id);
        if (isset($data) && $data->delete()){
            $status = (['status'=>'success']) ;
        }else{
            $status = (['status'=>'error']) ;
        }
        return $status;
    }
}
