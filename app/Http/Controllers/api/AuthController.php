<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $req){
        $email = $req->email;
        $password = $req->password;

        if (!empty($email) && !empty($password)){
            $user = User::where('email',$email)->first();
            if ($user){
                if (Hash::check($password,$user->password)){
                    $token = $user->createToken('altusToken')->plainTextToken;
                    return ['status'=>'success','user'=>$user,'token'=>$token];
                }else{
                    return ['status'=>'failed','msg'=>'Wrong Password'];
                }
            }else{
                return ['status'=>'failed','msg'=>'User does not exist'];
            }
        }else{
            return ['status'=>'failed','msg'=>'Please fill up email and password'];
        }
    }

    public function logout(Request $req){
        $req->user()->currentAccessToken()->delete();
        return ['status'=>'success','msg'=>'Terima kasih'];
    }
}
