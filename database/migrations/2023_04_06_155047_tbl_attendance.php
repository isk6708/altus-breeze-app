<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_attendance', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->date('date_in')->nullable();
            $table->date('date_out')->nullable();
            $table->string('location_in_gps')->nullable();
            $table->string('location_out_gps')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_attendance');
    }
};
