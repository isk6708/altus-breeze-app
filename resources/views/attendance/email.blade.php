Hi HOD,
<br>
Untuk Makluman, pegawai {{$attendance->getUser->name}} 
<br>
telah hadir seperti maklumat di bawah.
<br>
<br>

Tarikh: {{date('d-m-Y', strtotime($attendance->date_in))}}
<br>
Masa: {{date('h:i A', strtotime($attendance->date_in))}}
<br>

<br>
Sekian