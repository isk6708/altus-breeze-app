<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Altus - @yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Altus Breeze</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link dropdown-toggle" href="/attendance/list" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Balance Score Card
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/attendance/list">Goals</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="/attendance/list" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Attendance
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/attendance/list">Attendance List</a></li>
            <li><a class="dropdown-item" href="/attendance/capture">Capture My Attendance</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Attendance Approval</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/user">Manage User</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="/attendance/list" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Profile
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="{{ route('profile.edit') }}">Change Password</a></li>
            <li><hr class="dropdown-divider"></li>
            <li>              
            
            <form id="frmLogout" method="POST" action="{{ route('logout') }}" x-data>
                @csrf
                <x-dropdown-link href="javascript:document.getElementById('frmLogout').submit();">
                    Log Keluar
                </x-dropdown-link>
            </form>
            
          
            </li>
          </ul>
        </li>
        
      </ul>
      
    </div>
  </div>
</nav>

<div class="container">
            @yield('content')
        </div>
</body>
</html>