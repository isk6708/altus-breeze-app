@extends('layouts.admin')
@if (!isset($user->id))
    @section('title', 'Tambah Pengguna')
@else
    @section('title', 'Kemaskini Pengguna')
@endif
@section('content')
@if (!isset($user->id))
<h2>Tambah Pengguna</h2>
@else
<h2>Kemaskini Pengguna</h2>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (!isset($user->id))
<form action="/user" method="POST">
@else
<form action="/user/{{$user->id}}" method="POST">
    @method('PUT')
@endif

@csrf
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" class="form-control" name="email" value="{{$user->email}}"  id="exampleInputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>

  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name">
    
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" name="password" value="{{$user->password}}" class="form-control" id="exampleInputPassword1">
  </div>
  
  <div class="mb-3">
    <label for="confirm_password" class="form-label">Confirm Password</label>
    <input type="password" name="confirm_password" value="{{$user->password}}" class="form-control" id="confirm_password">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="/user" class="btn btn-danger">Back</a>



</form>
@endsection