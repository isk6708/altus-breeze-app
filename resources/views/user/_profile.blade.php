@extends('layouts.admin')
@section('title', 'Kemaskini Profil')
@section('content')
<h2>Kemaskini Profil</h2>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/user/update-my-profile" method="POST">
    @method('PUT')
@csrf
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" class="form-control" name="email" value="{{$user->email}}"  id="exampleInputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>

  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name">
    
  </div>
  <div class="mb-3">
    <label for="current_password" class="form-label">Current Password</label>
    <input type="password" name="current_password" class="form-control" id="current_password">
  </div>
  
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">New Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
  </div>
  
  <div class="mb-3">
    <label for="confirm_password" class="form-label">Confirm Password</label>
    <input type="password" name="confirm_password" class="form-control" id="confirm_password">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="/user" class="btn btn-danger">Back</a>



</form>
@endsection