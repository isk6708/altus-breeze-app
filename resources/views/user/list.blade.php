@extends('layouts.admin')
@section('title', 'Senarai Pengguna')
@section('content')
<h2>Senarai Pengguna</h2>
<form action="/user" class="col-6">
    @csrf
<div class="mb-3 row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <input type="email" class="form-control" name="email" id="email" value="{{$email}}">
  </div>

  <div class="mb-3 row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{$name}}">
  </div>

  
    <input type="submit" value="Teruskan" class="btn btn-primary">
    <a href="/user" class="btn btn-danger">Set Semula</a>
    
</form>

<a href="/user/create" class="btn btn-success">New User</a>

<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Tindakan</th>
    </tr>
    @php 
    $no = $users->firstItem();
    @endphp
    @foreach($users as $m)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$m->name}}</td>
        <td>{{$m->email}}</td>
        <td>

            
            <form action="/user/{{$m->id}}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/user/{{$m->id}}/edit" class="btn btn-secondary">Kemaskini</a>
                <input type="submit" onclick="return confirm('Anda Pasti')" class="btn btn-danger" value="Hapus">
            </form>
            

        </td>
    </tr>
    @endforeach

</table>
@php 

@endphp
{{ $users->appends(['name'=>$name,'email'=>$email])->links() }}
@endsection