<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AttendanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/attendance/list');
});

Route::get('/dashboard', function () {
    // return view('dashboard');
    return redirect('/attendance/list');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::prefix('attendance')->group(function () {
        Route::get('/list', 
            [AttendanceController::class,'list'])->name('list.attendance');
        Route::get('/capture/{userid?}', 
            [AttendanceController::class,'capture'])->name('capture.attendance');
        Route::get('/edit/{id}', 
            [AttendanceController::class,'edit'])->name('edit.attendance');
        Route::post('/save', 
            [AttendanceController::class,'save'])->name('save.attendance');
        Route::patch('/update/{id}', 
            [AttendanceController::class,'update'])->name('update.attendance');
        Route::delete('/delete/{id}', 
            [AttendanceController::class,'delete'])->name('delete.attendance');
    });
    Route::put('/user/update-my-profile',[UserController::class,'updateProfile']);
    Route::get('/user/my-profile',[UserController::class,'changePassword'])->name('user.my.profile');
    Route::resource('/user',UserController::class);

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
